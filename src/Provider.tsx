import { FC, createContext, useState, ReactNode, SetStateAction, Dispatch } from 'react';

export type Invoice = {
    invoiceId: string;
    amount: number;
}

const initialInvoice: Invoice = {
    invoiceId: '',
    amount: 0,
};

export const InvoiceContext = createContext<{invoice: Invoice, setInvoice: Dispatch<SetStateAction<Invoice>>}>({
    invoice: initialInvoice,
    setInvoice: () => {},
});

export const MyProvider: FC<{children: ReactNode}> = ({ children }) => {
    const [invoice, setInvoice] = useState<Invoice>(initialInvoice);

    return (
        <InvoiceContext.Provider value={{invoice, setInvoice}}>
            {children}
        </InvoiceContext.Provider>
    );
};
