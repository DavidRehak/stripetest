import { useContext, useState } from 'react';
import { InvoiceContext } from './Provider.tsx';

export default function CheckoutForm() {
    const [loading, setLoading] = useState(false);
    const {_,  setInvoice} = useContext(InvoiceContext);

    const handleSubmit = async (event) => {
        // We don't want to let default form submission happen here,
        // which would refresh the page.
        event.preventDefault();
        setLoading(true);
        const response = await fetch("/api/create-intent",
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST"
            })

        await response.json().then(value => {
            setInvoice(value)
            setLoading(false);
        });
    };

    if(loading) {
        return <>loading</>
    }

    return (
        <form onSubmit={handleSubmit}>
            <button type="submit">
                Submit Payment
            </button>
        </form>
    );
}