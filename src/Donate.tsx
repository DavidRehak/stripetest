import { FC } from 'react';
import { useStripe } from '@stripe/react-stripe-js';


const Donate: FC<{stripeUserId: string}> = ({ stripeUserId }) => {
    const stripe = useStripe();
    // 1. User clicks button
    const donate = async (amount: number) => {

        const fetchURL = "/api/create-intent";

        // 2. We call our server to get SESSION_ID
        const response = await fetch(fetchURL, {

            method: 'POST',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify({
                amount: amount * 1000,
                stripeUserId: stripeUserId
            })
        });

        if (!stripe) {
            return;
        }

        if(response.ok){

            const data      = await response.json();
            const sessionId = data.sessionId;

            // 4. We redirect ouser to Checkout
            const { error } = await stripe.redirectToCheckout({ sessionId: sessionId });
        }

    }

    return(
        <div className = 'Donate'>
            <button onClick = {() => donate(1)}>Donate</button>
        </div>
    );

}

export default Donate;