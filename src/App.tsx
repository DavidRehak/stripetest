import {Elements} from '@stripe/react-stripe-js';
import {loadStripe} from '@stripe/stripe-js';
import { useContext } from 'react';
import CheckoutForm from './CheckoutForm';
import InvoicePayment from './InvoicePayment.tsx';
import { InvoiceContext, MyProvider } from './Provider.tsx';
import Donate from './Donate.tsx';
// Make sure to call `loadStripe` outside of a component’s render to avoid
// recreating the `Stripe` object on every render.
const stripePromise = loadStripe('pk_test_51OcCWQE2rCt06vnASBzFzKRaazI0dXgbeQJs2Z5xeQAlt0oeQvhQ2hsxBmwBvYu6TSE5W9U5EyyYTS3zEJWTCira002WaeUU90');

function App() {
    return (
        <Elements stripe={stripePromise}>
            <MyProvider>
               {/* <CheckoutForm />*/}
               {/*<InvoicePayment />*/}
                <Donate />
            </MyProvider>
        </Elements>
    );
}

export default App;