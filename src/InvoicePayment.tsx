import React, { useState, useEffect, useContext } from 'react';
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import { InvoiceContext } from './Provider.tsx';

const InvoicePayment = () => {
    const stripe = useStripe();
    const elements = useElements();
    const [clientSecret, setClientSecret] = useState<string | null>(null);
    const {invoice} = useContext(InvoiceContext);

    useEffect(() => {
        // Fetch the client secret for the invoice from your server
        // This request should be made when the component mounts
        if(invoice.invoiceId) {
            console.log(invoice.invoiceId);
            fetchClientSecret();
        }
    }, [invoice.invoiceId]);

    const fetchClientSecret = async () => {
        // Make a request to your server to get the client secret for the invoice
        // Replace 'your-server-endpoint' with the actual endpoint on your server
        const response = await fetch(`/api/invoices/123/secret`,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify({
                    invoiceId: invoice.invoiceId
                })
            });
        const data = await response.json();
        setClientSecret(data.clientSecret);
    };

    const handlePayment = async (event: React.FormEvent) => {
        event.preventDefault();

        if (!stripe || !elements) {
            return;
        }

        // Use stripe.confirmCardPayment to confirm and process the payment
        const { error } = await stripe.confirmCardPayment(clientSecret!, {
            payment_method: {
                card: elements.getElement(CardElement)!,
            },
        });

        if (error) {
            // Handle payment error
            console.error(error.message);
        } else {
            // Payment succeeded, handle success
            console.log('Payment successful');
        }
    };

    return (
        <div>
            <div>
                Invoice Amount: ${invoice.amount}
            </div>
            <form onSubmit={handlePayment}>
                <CardElement />
                <button type="submit" disabled={!stripe}>
                    Pay Invoice
                </button>
            </form>
        </div>
    );
};

export default InvoicePayment;