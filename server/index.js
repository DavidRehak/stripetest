// eslint-disable-next-line @typescript-eslint/no-var-requires
const stripe = require("stripe")("sk_test_51OcCWQE2rCt06vnAzd4zptPM3B0EFcOOJ0MNxWn0bxqMsFGO9ENLjd7XtcKywGZU4t36bfnKu6xMhxt8PD9Mu0pE00jGVnvncN");
const express = require('express');
const app = express();

app.use(express.json());

app.use('/create-intent', async (req, res, next) => {

    let amount       = req.body.amount;
    let stripeUserId = req.body.stripeUserId;

    const session = await stripe.checkout.sessions.create({
        payment_method_types: ['card'],
        line_items: [{
            price_data: {
                product_data: {
                    name: 'Donation'
                },
                unit_amount: amount,
                currency: 'usd'
            },
            quantity: 1
        }],
        mode: 'payment',
        payment_intent_data: {
            transfer_data: {
                destination: stripeUserId
            },
        },
        invoice_creation: {
          enabled: true
        },
        success_url: 'http://localhost:3000/success',
        cancel_url: 'http://localhost:3000/cancel'
    });

    let sessionId = session.id;

    sessionId
        ? res.status(200).json({sessionId: sessionId})
        : res.status(500).send('Error!');
});

app.listen(8000, () => {
    console.log('Running on port 8000');
});

// {
//     amount: 1099,
//         currency: 'usd',
//     payment_method: ['card'],
//     // account: "acct_1OcCWQE2rCt06vnA",
//     // In the latest version of the API, specifying the `automatic_payment_methods` parameter is optional because Stripe enables its functionality by default.
//     automatic_payment_methods: {enabled: true},
// }